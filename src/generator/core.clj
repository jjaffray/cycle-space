(ns generator.core
  [:use [clojure.set :only [union difference intersection]]]
  (:gen-class))

(def basis
  #{#{#{1 2} #{2 3} #{3 1}}
    #{#{2 3} #{3 4} #{2 4}}
    #{#{3 4} #{4 5} #{3 5}}
    #{#{4 5} #{5 1} #{1 4}}
    #{#{5 1} #{1 2} #{2 5}}
    #{#{1 2} #{2 3} #{3 4} #{4 5} #{5 1}}})

(defn power-set [s]
  (if (empty? s) #{#{}}
    (let [without-first (power-set (rest s))]
      (into without-first (map #(conj % (first s)) without-first)))))

(defn symmetric-difference [s]
  (if (empty? s) #{}
  (reduce #(difference (union %1 %2) (intersection %1 %2)) s)))

(defn graph [edges]
  (str
  "\\begin{tikzpicture}
\\foreach \\number in {1,...,5}{
      % Computer angle:
        \\mycount=\\number
        \\advance\\mycount by -1
  \\multiply\\mycount by 72
        \\advance\\mycount by 18
      \\node[draw,circle,inner sep=0.25cm] (N-\\number) at (\\the\\mycount:5.4cm) {};
    }
"
  (apply str (map #(str "\\path (N-" (first %) ") edge[-] (N-" (second %) ");") edges))
"
\\end{tikzpicture}
  "))

(defn elements [basis]
  (map symmetric-difference (power-set basis)))

(defn -main
  [& args]
  (println (map graph (elements basis))))
